import React, { Component } from "react";
import Aux from "../Auxe/Auxiliary";
import Modal from "../../components/UI/Modal/Modal";

const withErrorsController = (WrappedConmonent, axios) => {
  return class extends Component {
    state = {
      error: null
    };
    componentWillMount() {
      //clear all of error when request
      this.requestInterceptor = axios.interceptors.request.use(req => {
        this.setState({ error: null });
        return req;
      });

      this.responseInterceptor = axios.interceptors.response.use(
        res => res,
        error => {
          this.setState({ error: error });
        }
      );
    }

    componentWillUnmount() {
      axios.interceptors.request.eject(this.requestInterceptor);
      axios.interceptors.response.eject(this.responseInterceptor);
    }

    errorClosedHandaler = () => {
      this.setState({ error: null });
    };

    render() {
      return (
        <Aux>
          <Modal show={this.state.error} clicked={this.errorClosedHandaler}>
            {this.state.error ? this.state.error.message : null}
          </Modal>
          <WrappedConmonent {...this.props} />
        </Aux>
      );
    }
  };
};

export default withErrorsController;
