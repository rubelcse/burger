import React, { Component } from "react";
import Aux from "../Auxe/Auxiliary";
import Styles from "./Layout.module.css";
import Toolbar from "../../components/UI/Navigation/Toolbar/Toolbar";
import SideDrawer from "../../components/UI/Navigation/SideDrawer/SideDrawer";

class Layout extends Component {
  state = {
    sideDrawer: false
  };

  showSideDrawerHandaler = () => {
    this.setState({ sideDrawer: false });
  };

  sideDrawerHandaler = () => {
    this.setState(prevState => {
      return { sideDrawer: !prevState.sideDrawer };
    });
  };

  render() {
    return (
      <Aux>
        <Toolbar sideDrawerHandaler={this.sideDrawerHandaler} />
        <SideDrawer
          open={this.state.sideDrawer}
          close={this.showSideDrawerHandaler}
        />
        <main className={Styles.Content}>{this.props.children}</main>
      </Aux>
    );
  }
}
export default Layout;
