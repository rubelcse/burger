import React, { Component } from "react";
import Aux from "../../hoc/Auxe/Auxiliary";
import Button from "../UI/Button/Button";

class OrderSummary extends Component {
  //This should be a functional component, not needed to class component.only using form debugging purpose.
  componentWillUpdate() {
    console.log("[Ordersummery] wiiupdate");
  }
  render() {
    const ingridentSummary = Object.keys(this.props.ingridients).map(igKey => {
      return (
        <li key={igKey}>
          <span style={{ textTransform: "capitalize" }}>{igKey}</span> :{" "}
          {this.props.ingridients[igKey]}
        </li>
      );
    });

    return (
      <Aux>
        <h3>Your Order</h3>
        <p>A Delicious Burger With Following Ingridients :</p>
        <ul>{ingridentSummary}</ul>
        <p>
          <strong>Total Price : {this.props.price.toFixed(2)}</strong>
        </p>
        <p>Continue to check out?</p>
        <Button btnType="Danger" clicked={this.props.purchaseCancel}>
          CANCEL
        </Button>
        <Button btnType="Success" clicked={this.props.purchaseContinue}>
          CONTINUE
        </Button>
      </Aux>
    );
  }
}
export default OrderSummary;
