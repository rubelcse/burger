import React from "react";
import Styles from "./Burger.module.css";
import BurgerIngridient from "./BurgerIngridient/BurgerIngridient";

const burger = props => {
  let transformIngdients = Object.keys(props.ingridients)
    .map(igKey => {
      return [...Array(props.ingridients[igKey])].map((_, index) => {
        return <BurgerIngridient key={igKey + index} type={igKey} />;
      });
    })
    .reduce((ara, el) => {
      //return only one array into an map multidimension
      return ara.concat(el);
    });

  if (transformIngdients.length === 0) {
    transformIngdients = <p>Plase select an ingridients!!</p>;
  }

  return (
    <div className={Styles.Burger}>
      <BurgerIngridient type="bread-top" />
      {transformIngdients}
      <BurgerIngridient type="bread-bottom" />
    </div>
  );
};

export default burger;
