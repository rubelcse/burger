import React from "react";
import Styles from "./BuildControls.module.css";
import BuildControl from "./BuildControl/BuildControl";

const BuildControls = props => {
  const controls = [
    { label: "Salad", type: "salad" },
    { label: "Bacon", type: "bacon" },
    { label: "Cheese", type: "cheese" },
    { label: "Meat", type: "meat" }
  ];
  return (
    <div className={Styles.BuildControls}>
      <p>
        Current Price : <strong>{props.price.toFixed(2)}</strong>
      </p>
      {controls.map(ctrl => (
        <BuildControl
          key={ctrl.label}
          label={ctrl.label}
          added={() => props.ingridientAdded(ctrl.type)}
          remove={() => props.ingridientRemove(ctrl.type)}
          disabled={props.disabled[ctrl.type]}
        />
      ))}
      <button
        className={Styles.OrderButton}
        disabled={!props.purchasable}
        onClick={props.orderd}
      >
        ORDER NOW
      </button>
    </div>
  );
};

export default BuildControls;
