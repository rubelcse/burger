import React, { Component } from "react";
import Styles from "./BurgerIngridient.module.css";
import PropTypes from "prop-types";

class BurgerIngridient extends Component {
  render() {
    let ingridient = null;
    switch (this.props.type) {
      case "bread-bottom":
        ingridient = <div className={Styles.BreadBottom} />;
        break;
      case "bread-top":
        ingridient = (
          <div className={Styles.BreadTop}>
            <div className={Styles.Seeds1} />
            <div className={Styles.Seeds2} />
          </div>
        );
        break;
      case "meat":
        ingridient = <div className={Styles.Meat} />;
        break;
      case "cheese":
        ingridient = <div className={Styles.Cheese} />;
        break;
      case "bacon":
        ingridient = <div className={Styles.Bacon} />;
        break;
      case "salad":
        ingridient = <div className={Styles.Salad} />;
        break;
      default:
        ingridient = null;
    }

    return ingridient;
  }
}

BurgerIngridient.propTypes = {
  type: PropTypes.string.isRequired
};

export default BurgerIngridient;
