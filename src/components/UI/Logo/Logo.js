import React from "react";
import Styles from "./Logo.module.css";
import LogoImages from "../../../assets/images/burger-logo.png";

const logo = props => {
  return (
    <div className={Styles.Logo}>
      <img src={LogoImages} alt="MyBurger" />
    </div>
  );
};

export default logo;
