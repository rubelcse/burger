import React from "react";
import Burger from "../../../Burger/Burger";
import Button from "../../Button/Button";
import Styles from "./CheckoutSummary.module.css";

const checkoutSummary = props => {
  return (
    <div className={Styles.CheckoutSummary}>
      <h1>We Hope It Very Testi!</h1>
      <div style={{ width: "300px", height: "300px", margin: "auto" }}>
        <Burger ingridients={props.ingridients} />
      </div>
      <Button btnType="Danger" clicked>
        Cancel
      </Button>
      <Button btnType="Success" clicked>
        Continue
      </Button>
    </div>
  );
};

export default checkoutSummary;
