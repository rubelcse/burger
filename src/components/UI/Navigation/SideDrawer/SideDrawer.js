import React from "react";
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import Styles from "./SideDrawer.module.css";
import BackDrop from "../../Backdrop/Backdrop";
import Aux from "../../../../hoc/Auxe/Auxiliary";

const sideDrawer = props => {
  let attachedClass = [Styles.SideDrawer, Styles.Close];
  if (props.open) {
    attachedClass = [Styles.SideDrawer, Styles.Open];
  }
  return (
    <Aux>
      <BackDrop show={props.open} clicked={props.close} />
      <div className={attachedClass.join(" ")}>
        <div className={Styles.Logo}>
          <Logo />
        </div>

        <nav>
          <NavigationItems />
        </nav>
      </div>
    </Aux>
  );
};

export default sideDrawer;
