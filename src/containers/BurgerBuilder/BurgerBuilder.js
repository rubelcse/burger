import React, { Component } from "react";
import Aux from "../../hoc/Auxe/Auxiliary";
import Burger from "../../components/Burger/Burger";
import BuilderControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import Spinner from "../../components/UI/Spinner/Spinner";
import withErrorsHandaler from "../../hoc/withErrorsController/withErrorsController";
import axios from "../../axios-orders";

const INGRIDIENT_PRICES = {
  salad: 0.5,
  bacon: 0.7,
  cheese: 0.4,
  meat: 1.3
};

class BurgerBuilder extends Component {
  state = {
    ingridients: null,
    totalPrice: 4,
    purchasable: false,
    purchasing: false,
    loading: false,
    error: false
  };

  componentDidMount() {
    axios
      .get("/ingridients")
      .then(response => this.setState({ ingridients: response.data }))
      .catch(errors => {
        this.setState({ error: true });
      });
  }

  updatePurchaseState(ingridients) {
    const sum = Object.keys(ingridients)
      .map(igKey => {
        return ingridients[igKey];
      })
      .reduce((sum, el) => {
        return sum + el;
      });
    this.setState({ purchasable: sum > 0 });
  }

  purchaseHandaler = () => {
    this.setState({ purchasing: true });
  };

  cancelPurchaseHandaler = () => {
    this.setState({ purchasing: false });
  };

  purchaseContinueHandaler = () => {
    this.setState({ loading: true });
    const order = {
      ingridients: this.state.ingridients,
      price: this.state.totalPrice,
      customer: {
        name: "Rubel ahmed",
        address: {
          street: "Test street 1",
          zipcode: "4123",
          country: "Bangladesh"
        },
        email: "rubel@gmail.com"
      },
      deliveryMethod: "fastest"
    };
    axios
      .post("/orders", order)
      .then(response => {
        this.setState({ loading: false, purchasing: false });
        console.log(response);
      })
      .catch(errors => {
        this.setState({ loading: false, purchasing: false });
        console.log(errors);
      });
  };

  addIngridientHandaler = type => {
    const oldCount = this.state.ingridients[type];
    const updateCount = oldCount + 1;
    const updateIngridients = { ...this.state.ingridients };
    updateIngridients[type] = updateCount;
    const priceAddition = INGRIDIENT_PRICES[type];
    const oldPrice = this.state.totalPrice;
    const newPrice = oldPrice + priceAddition;
    this.setState({ ingridients: updateIngridients, totalPrice: newPrice });
    this.updatePurchaseState(updateIngridients);
  };

  removeIngridientHandalaer = type => {
    const oldCount = this.state.ingridients[type];
    if (oldCount <= 0) {
      return;
    }
    const updateCount = oldCount - 1;
    const updateIngridients = { ...this.state.ingridients };
    updateIngridients[type] = updateCount;
    const priceDiduction = INGRIDIENT_PRICES[type];
    const oldPrice = this.state.totalPrice;
    const newPrice = oldPrice - priceDiduction;
    this.setState({ ingridients: updateIngridients, totalPrice: newPrice });
    this.updatePurchaseState(updateIngridients);
  };

  render() {
    const disableInfo = { ...this.state.ingridients };
    for (let key in disableInfo) {
      disableInfo[key] = disableInfo[key] <= 0;
    }

    let orderSummery = null;
    let burger = this.state.error ? (
      <p>Ingridients can't be loaded</p>
    ) : (
      <Spinner />
    );

    if (this.state.ingridients) {
      burger = (
        <Aux>
          <Burger ingridients={this.state.ingridients} />
          <BuilderControls
            ingridientAdded={this.addIngridientHandaler}
            ingridientRemove={this.removeIngridientHandalaer}
            disabled={disableInfo}
            purchasable={this.state.purchasable}
            orderd={this.purchaseHandaler}
            price={this.state.totalPrice}
          />
        </Aux>
      );
      orderSummery = (
        <OrderSummary
          ingridients={this.state.ingridients}
          price={this.state.totalPrice}
          purchaseCancel={this.cancelPurchaseHandaler}
          purchaseContinue={this.purchaseContinueHandaler}
        />
      );
    }

    if (this.state.loading) {
      orderSummery = <Spinner />;
    }

    return (
      <Aux>
        <Modal
          show={this.state.purchasing}
          closeModal={this.cancelPurchaseHandaler}
        >
          {orderSummery}
        </Modal>
        {burger}
      </Aux>
    );
  }
}

export default withErrorsHandaler(BurgerBuilder, axios);
