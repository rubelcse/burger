import React, { Component } from "react";
import CheckoutSummary from "../../components/UI/Order/CheckoutSummary/CheckoutSummary";

class Checkout extends Component {
  state = {};
  render() {
    return (
      <div>
        <CheckoutSummary />
      </div>
    );
  }
}

export default Checkout;
