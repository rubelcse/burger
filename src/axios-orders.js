import axios from "axios";

const instance = axios.create({
  baseURL: "http://eapi.me/api"
});

export default instance;
